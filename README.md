RedmineBot
----------
### For ENGLISH README please [click here](https://bitbucket.org/rami_dk/redmine_bot/src/master/README_ENG.md)
Бот, напоминающий пользователю, отметить затраченное на задачу время. Для системы управления проектами **Redmine** (www.redmine.org)

Уведомления отправляются через **Slack**.

### Триггеры для отправки уведомлений: ###
* Пользователь создал задачу, но не отметил время.
* Пользователь изменил исполнителя задачи, но не отметил время.

Для корректной работы уведомлений воркфлоу подразумевает измененние исполнителя, если задача переходит в работу другому человеку.

* Например: Разработчик завершил работу на задачей: изменил статус задачи на "Проверка" и изменил исполнителя на тимлида, но не отметил затраченное время. Разработчик получит уведомление.

### Установка: ###
**RedmineBot** мониторит все проекты Redmine в которых включен модуль **"time_tracking"**.

Шаблон файла настроек ```local_settings.example```

* REDMINE SETTINGS:
    * REDMINE_TOKEN: API токен пользователя редмайн с правами администратора.
    * REDMINE_BASE_URL: url вашего Redmine
    * DEFAULT_DAYS_OFFSET: Количество дней за которые будут отслеживаться изменения задач.
    * USERS_TO_NOTIFY: Список email пользователей Redmine, которые будут получать уведомления. (Для корректной отправки уведомлений через Slack, email указанные в профилях пользователей в Redmine, должнен совпадать с email в профилях Slack)
    * USERS_TO_NOT_NOTIFY: Список email пользователей Redmine, которые не будут получать уведомления.

* SLACK SETTINGS:
    * SLACK_API_TOKEN: API токен Slack бота (https://api.slack.com/docs/token-types#bot). Подробнее о настройке бота в Slack - https://api.slack.com/bot-users .

* INTERNATIONALIZATION SETTINGS
    * LOCALE: локаль (доступные локали: 'en', 'ru')

### Пример файла настроек: ###
```
### REDMINE SETTINGS ###

REDMINE_TOKEN = 'dc34c4a3ce46aa129231b3b45649d67ad3422e54'
REDMINE_BASE_URL = 'http://redmine.my-company.com'

DEFAULT_DAYS_OFFSET = 1

USERS_TO_NOTIFY = ['tom@my-company.com', kris@my-company.com]


### SLACK SETTINGS ###

SLACK_API_TOKEN = 'xoxb-325679341639-bzJwW2217DyjnQMVCkRL0SrF'


### LOGGING SETTINGS ###

LOGGING_LEVEL = 'DEBUG'


### INTERNATIONALIZATION SETTINGS ###

LOCALE_PATH = os.path.join(BASE_DIR, 'locales')
LOCALE = 'en'  # Available translations: 'en', 'ru'
```

### Запуск: ###
```python main.py```

Наиболее удобно настроить запуск cron-ом например раз в сутки (указав DEFAULT_DAYS_OFFSET = 1)

### Пример уведомления Slack: ###
![Уведомление в Slack](https://api.monosnap.com/rpc/file/download?id=ytPFGaFX3YZ2JF8HKr19XqWpo1MJth)