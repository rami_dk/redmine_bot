import datetime
import settings


def get_date_by_offset():
    """Current date minus days offset specified in settings DEFAULT_DAYS_OFFSET -
    the period for which we need track changes made in issues"""
    today = datetime.datetime.now().date()
    date_by_offset = datetime.datetime(today.year, today.month, today.day) - datetime.timedelta(
        days=settings.DEFAULT_DAYS_OFFSET)
    return date_by_offset
