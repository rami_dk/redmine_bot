import abc

import datetime

from redmine.utils import get_date_by_offset


class BaseObject(metaclass=abc.ABCMeta):
    """Base abstract object class for all objects parsed from API responses"""
    response_field = None  # Field in response that corresponds to the object type
    related_parse = {}  # Fields in response that will also be deserialized and linked to current object
    related_raw = ['id', 'mail']  # Fields in response that will be linked to current object without deserialization
    object_name = None  # Name that represents this object type in other objects
    related_name = ''  # Name used in related objects to represent current one (similar to Django FK "related_name")

    def __init__(self):
        from redmine.data import redmine_data
        self.redmine_data = redmine_data

    def __repr__(self):
        return '<{} id: {}>'.format(self.__class__.__name__, self.id)

    def __str__(self):
        return '{} id: {}'.format(self.__class__.__name__, self.id)

    def __eq__(self, other):
        return self.__class__ == other.__class__ and self.id == other.id

    def __hash__(self):
        return hash(self.id)


class User(BaseObject):
    response_field = 'users'
    object_name = 'user'
    related_name = 'users'

    def time_entry_requiring_issues(self):
        """Returns user issues for which time_entry needs to be created"""
        return [x for x in self.redmine_data.issues() if x.needs_time_entry(user=self)]


class Issue(BaseObject):
    response_field = 'issues'
    object_name = 'issue'
    related_name = 'issues'
    related_parse = {'author': 'users', 'project': 'projects'}
    related_raw = BaseObject.related_raw + ['created_on', ]

    def journals(self):
        """Journals of the current issue"""
        return self.redmine_data.journals(issue_id=self.id)

    def needs_time_entry(self, user):
        """Defines if the issue needs time_entry to be created"""
        time_entries = filter_by_related(filter_by_related(self.redmine_data.time_entries(), self), user)
        interesting_changes_count = len(self.interesting_changes(user))
        if self.is_created(user):
            # Journal record doesn't get created when the Task is CREATED (not UPDATED)
            # So we add 1 - because time_entry is also needed for the CREATED Task.
            interesting_changes_count += 1
        return interesting_changes_count > len(time_entries)

    def interesting_changes(self, user):
        """Returns all journal records that show that:
            * assigned user has changed
            * journal record was created in a valid period of time
            """
        return [x for x in filter_by_related(self.journals(), user) if
                x.assigned_to_changed_to_other_user(user) and x.valid_for_period()]

    def valid_for_period(self):
        """True if journal record was created within a time period specified in settings.DEFAULT_DAYS_OFFSET"""
        date_by_offset = get_date_by_offset()
        return datetime.datetime.strptime(self.created_on, '%Y-%m-%dT%H:%M:%SZ') >= date_by_offset

    def is_created(self, user):
        """True if the Task was CREATED in specified period of time by the user"""
        return self.valid_for_period() and (self.author == user)


class Journal(BaseObject):
    response_field = 'issue__journals'
    related_parse = {'issue': 'issues', 'user': 'users'}
    related_raw = BaseObject.related_raw + ['details', 'created_on']

    def assigned_to_changed_to_other_user(self, old_user):
        """True if the assigned user has changed from old_user"""
        assigned_to_id_changed = False
        for detail in self.details:
            if detail.get('name') == 'assigned_to_id' and detail.get('old_value') == str(old_user.id):
                assigned_to_id_changed = True
                break

        return assigned_to_id_changed

    def valid_for_period(self):
        """True if journal record was created within a time period specified in settings"""
        date_by_offset = get_date_by_offset()
        return datetime.datetime.strptime(self.created_on, '%Y-%m-%dT%H:%M:%SZ') >= date_by_offset


class TimeEntry(BaseObject):
    response_field = 'time_entries'
    related_parse = {'issue': 'issues', 'user': 'users'}


class Project(BaseObject):
    response_field = 'projects'
    related_raw = BaseObject.related_raw + ['name', 'enabled_modules']


def filter_by_related(objects_list, instance):
    """Returns those objects from object_list that correspond to instance"""
    return [x for x in objects_list if getattr(x, instance.object_name) == instance]
