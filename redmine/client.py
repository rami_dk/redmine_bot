import datetime

import requests

import settings
from redmine.decorators import parse_response
from redmine.objects import User, Issue, Journal, TimeEntry, Project
from redmine.utils import get_date_by_offset


class RedmineClient(object):
    """Responsible for interacting with Redmine API"""
    def __init__(self):
        self.headers = {'X-Redmine-API-Key': settings.REDMINE_TOKEN}
        self.base_url = settings.REDMINE_BASE_URL
        self.response_format = 'json'
        self.params = {'limit': 100}

        # API uses different time formats
        date_by_offset = get_date_by_offset()
        self.date_by_offset_iso = datetime.datetime.isoformat(date_by_offset).split('.')[0] + 'Z'
        self.date_by_offset_simple = date_by_offset.strftime('%Y-%m-%d')

    def _request(self, url, type_, data=None, headers=None, files=None, params={}):
        """Performs API calls"""
        session = requests.Session()
        session.headers.update(self.headers)
        params.update(self.params)

        if headers:
            session.headers.update(headers)

        response = getattr(session, type_.lower())(url, data=data, files=files, params=params)
        response.raise_for_status()

        return response.json()

    @parse_response(Issue)
    def get_project_issues(self, project_id):
        """Calls API for all issues for corresponding project_id
         that were updated after self.date_by_offset_iso"""
        params = {
            'updated_on': '>=%s' % self.date_by_offset_iso,
            'project_id': project_id
        }
        url = self.base_url + '/issues.{}'.format(self.response_format)
        return self._request(url, 'GET', params=params)

    def get_issues(self, project_ids):
        """Gets all issues for specified project_ids"""
        issues = []
        for project_id in project_ids:
            issues.extend(self.get_project_issues(project_id))
        return issues

    def get_issue(self, issue_id, params):
        """Calls API to get issue"""
        url = self.base_url + '/issues/{}.{}'.format(issue_id, self.response_format)
        return self._request(url, 'GET', params=params)

    @parse_response(Journal)
    def get_journals(self, issue_id):
        """Gets journals for corresponding issue_id"""
        params = {
            'include': 'journals'
        }
        response = self.get_issue(issue_id, params)

        for journal in response['issue']['journals']:
            journal.update({'issue': {'id': issue_id}})
        return response

    @parse_response(TimeEntry)
    def get_time_entries_by_project_id(self, project_id):
        """Calls API to get time_entries for corresponding project_id
        that were created after self.date_by_offset_iso"""
        params = {
            'spent_on': '><%s' % self.date_by_offset_simple,
            'project_id': project_id
        }
        url = self.base_url + '/time_entries.{format}'.format(format=self.response_format)
        return self._request(url, 'GET', params=params)

    def get_time_entries(self, projects):
        """Gets all time_entries for specified project_ids"""
        time_entry = []
        for project in projects:
            time_entry.extend(self.get_time_entries_by_project_id(project.id))
        return time_entry

    @parse_response(User)
    def get_users(self):
        """Calls API to get all users"""
        url = self.base_url + '/users.{}'.format(self.response_format)
        return self._request(url, 'GET')

    def _get_projects(self, params_=None):
        """Calls API to get projects"""
        params = {
            'include': 'enabled_modules'
        }
        if params_:
            params.update(params_)
        url = self.base_url + '/projects.{}'.format(self.response_format)
        return self._request(url, 'GET', params=params)

    @parse_response(Project)
    def get_projects(self):
        """Iterates through pages to get all projects"""
        offset = 0
        projects = self._get_projects()
        projects_ = projects['projects']
        while projects_:
            offset += 100
            projects_ = self._get_projects(params_={'offset': offset})['projects']
            projects['projects'].extend(projects_)

        return projects


redmine_client = RedmineClient()
