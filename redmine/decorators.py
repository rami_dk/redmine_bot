import functools


def create_instance_or_none(class_, instance_data):
    """Creates instance of passed class_ from data received from API"""
    instance = class_()
    for field, value in instance_data.items():
        if field in instance.related_raw:
            setattr(instance, field, value)
        elif field in instance.related_parse:
            from redmine.data import redmine_data
            objects = getattr(redmine_data, instance.related_parse[field])()
            matched_object = [x for x in objects if value['id'] == x.id]

            if matched_object:
                setattr(instance, field, matched_object[0])
            else:
                # print('{} {}. Не удалось сопоставить для {} id: {}'
                #       .format(instance.__class__.__name__, instance.id, field, value['id']))
                del instance
                return

    return instance


def get_data(result, field_path):
    """Returns data that matches field_path"""
    if not field_path:
        raise Exception('"field_path" should contain path')

    data = result
    for field in field_path:
        data = data[field]
    return data


def parse_response(class_):
    """Decorator for parsing API responses data to objects"""
    def decorator(function):
        @functools.wraps(function)
        def wrapper(*args, **kwargs):
            result = function(*args, **kwargs)
            field_path = class_.response_field.split('__')
            data = get_data(result=result, field_path=field_path)

            parsed_instances = []
            for instance_data in data:
                instance = create_instance_or_none(class_, instance_data)
                if instance:
                    parsed_instances.append(instance)

            return parsed_instances
        return wrapper
    return decorator


class SingletonDecorator:
    def __init__(self, klass):
        self.klass = klass
        self.instance = None

    def __call__(self, *args, **kwargs):
        if self.instance is None:
            self.instance = self.klass(*args, **kwargs)
        return self.instance


def cached(function):
    """Caches data in private variables
    Forces update if refresh=True"""
    @functools.wraps(function)
    def wrapper(*args, **kwargs):
        private_attr = '__{}'.format(function.__name__)
        self_ = args[0]

        if not hasattr(self_, private_attr):
            setattr(self_, private_attr, None)

        if kwargs.get('refresh', False) or getattr(self_, private_attr) is None:
            result = function(*args, **kwargs)
            setattr(self_, private_attr, result)

        return getattr(self_, private_attr)
    return wrapper
