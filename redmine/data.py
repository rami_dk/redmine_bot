from redmine.decorators import SingletonDecorator, cached
import settings


@SingletonDecorator
class RedmineData(object):
    """Storage class for objects received from Redmine API.
    "refresh" flag can be passed to methods with @cashed decorator to force retrieving from API"""
    def __init__(self):
        from redmine.client import redmine_client
        self.client = redmine_client
        self.__journals = None

    @cached
    def issues(self, refresh=False):
        """Returns all issues"""
        return self.client.get_issues(project_ids=[x.id for x in self.projects()])

    @cached
    def users(self, refresh=False):
        """Returns all users"""
        return self.client.get_users()

    def users_to_notify(self):
        """Returns users that need to be notified"""
        all_users = self.users()

        USERS_TO_NOTIFY = getattr(settings, 'USERS_TO_NOTIFY', [])
        USERS_TO_NOT_NOTIFY = getattr(settings, 'USERS_TO_NOT_NOTIFY', [])
        if USERS_TO_NOTIFY and USERS_TO_NOT_NOTIFY:
            raise Exception('Can\'t set both: USERS_TO_NOTIFY and USERS_TO_NOT_NOTIFY')

        if not USERS_TO_NOTIFY and not USERS_TO_NOT_NOTIFY:
            return all_users
        if USERS_TO_NOTIFY:
            return [x for x in all_users if x.mail in USERS_TO_NOTIFY]
        if USERS_TO_NOT_NOTIFY:
            return [x for x in all_users if x.mail not in USERS_TO_NOT_NOTIFY]

    def user_emails(self):
        """Returns emails for users that need to be notified"""
        return [x.mail for x in self.users()]

    @cached
    def time_entries(self, refresh=False):
        """Returns all time entries for all projects"""
        return self.client.get_time_entries(projects=self.projects())

    def journals(self, issue_id, refresh=False):
        """Returns journals by issue id"""
        if refresh:
            self.__journals = None
        if self.__journals:
            by_issue_id = [x for x in self.__journals if issue_id == x.issue.id]
            if by_issue_id:
                return by_issue_id

        by_issue_id = self.client.get_journals(issue_id)
        if self.__journals is not None:
            self.__journals.extend(by_issue_id)
        else:
            self.__journals = by_issue_id
        return by_issue_id

    @cached
    def projects(self, refresh=False):
        """Returns projects that have "time_tracking" option enabled."""
        projects = self.client.get_projects()

        def time_tracking_enabled(project):
            is_time_tracking_enabled = False
            for module in project.enabled_modules:
                if module.get('name') == 'time_tracking':
                    is_time_tracking_enabled = True
                    break

            return is_time_tracking_enabled

        return [x for x in projects if time_tracking_enabled(x)]


redmine_data = RedmineData()
