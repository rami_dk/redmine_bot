import logging

from slackclient import SlackClient

import settings
from redmine.decorators import cached

sc = SlackClient(settings.SLACK_API_TOKEN)


class CustomSlackClient(object):
    """Responsible for interacting with Slack API.
    "refresh" flag can be passed to methods with @cashed decorator to force retrieving from API"""
    def __init__(self, client):
        self.client = client

    def send_message(self, username, text):
        """Sends message to user."""
        return self.client.api_call(
            'chat.postMessage',
            channel=username,
            text=text
        )

    @cached
    def users(self, refresh=False):
        """Lists all users in a Slack team."""
        return self.client.api_call('users.list')['members']

    def get_username_by_email(self, email):
        """Returns username by email."""
        slack_users = self.users()
        username = [x['name'] for x in slack_users if x['profile'].get('email') == email]
        if username:
            return username[0]
        logging.critical('Slack user with email "{}" not found!'.format(email))


slack_client = CustomSlackClient(sc)
