import datetime
import json
import random
from collections import defaultdict, Counter

import os

import settings


class IssuesMessage(object):
    """Slack message with list of issues that need time entries."""
    def __init__(self, issues, text):
        self.issues = issues
        self.text = text
        self.__emogis = []

    @staticmethod
    def group_by_project(issues):
        """Grouping issues by project."""
        grouped_by_projects = defaultdict(list)
        for issue in issues:
            grouped_by_projects[issue.project].append(issue)

        return grouped_by_projects

    def random_emogi(self):
        if self.__emogis:
            return ':{}:'.format(random.choice(list(self.__emogis.keys())))

        __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
        with open(os.path.join(__location__, 'slack_emogi_list.json')) as f:
            self.__emogis = json.loads(f.read())

        return ':{}:'.format(random.choice(list(self.__emogis.keys())))

    def prepared(self):
        """Prepares content of the message and returns it."""
        date_text = _('*date:* {}').format(datetime.datetime.now().date())
        msg = [self.text, date_text]

        grouped_by_project = self.group_by_project(issues=self.issues)

        for project, issues in grouped_by_project.items():

            grouped_by_issue = Counter(issues)
            project_text = _('*project:* "{project}"').format(project=project.name)
            msg.append(project_text)

            for issue, count in grouped_by_issue.items():
                link = settings.REDMINE_BASE_URL + '/{}/{}'.format(issue.response_field, issue.id)
                text = '    ' + link + _(' Time entries required for all {} task updates.').format(count)
                msg.append(text)

        msg.append(self.random_emogi())

        return '\n'.join(msg)
