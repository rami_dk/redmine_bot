RedmineBot
----------
A bot reminding the user to mark the time spent on the task. For project management system ** Redmine ** (www.redmine.org)

Notifications sent via **Slack**.

### Triggers for sending notifications: ###
* User created a task but did not mark the time..
* User changed the task performer ("assigned_to") but did not mark the time.

For the correct operation of the notifications, the workflow implies the change of the performer ("assigned_to") if the task goes to work for another person.

* For example: The developer has completed work on the task: changed the status of the task to "Testing" and reassigned the task to the team lead, but did not mark the spent time. The developer will receive a notification.

### Installation: ###
**RedmineBot** monitors all Redmine projects where the module **"time_tracking"** is enabled.

Settings file example ```local_settings.example```

* REDMINE SETTINGS:
    * REDMINE_TOKEN: Redmine user API token with administrator permissions.
    * REDMINE_BASE_URL: your Redmine url.
    * DEFAULT_DAYS_OFFSET: The number of days for which task changes will be tracked.
    * USERS_TO_NOTIFY: List of Redmine user email addresses who will receive notifications. (For correct sending of notifications via Slack, the emails specified in the user profiles in Redmine must match the email in the Slack profiles).
    * USERS_TO_NOT_NOTIFY: List of Redmine user email addresses who will not receive notifications.

* SLACK SETTINGS:
    * SLACK_API_TOKEN: Slack bot API token (https://api.slack.com/docs/token-types#bot). How to set up Slack bot - https://api.slack.com/bot-users .

* INTERNATIONALIZATION SETTINGS
    * LOCALE: active locale (available locales: 'en', 'ru')

### Settings file example: ###
```
### REDMINE SETTINGS ###

REDMINE_TOKEN = 'dc34c4a3ce46aa129231b3b45649d67ad3422e54'
REDMINE_BASE_URL = 'http://redmine.my-company.com'

DEFAULT_DAYS_OFFSET = 1

USERS_TO_NOTIFY = ['tom@my-company.com', kris@my-company.com]


### SLACK SETTINGS ###

SLACK_API_TOKEN = 'xoxb-325679341639-bzJwW2217DyjnQMVCkRL0SrF'


### LOGGING SETTINGS ###

LOGGING_LEVEL = 'DEBUG'


### INTERNATIONALIZATION SETTINGS ###

LOCALE_PATH = os.path.join(BASE_DIR, 'locales')
LOCALE = 'en'  # Available translations: 'en', 'ru'
```

### Run: ###
```python main.py```

The most convenient way is to configure cron launch, for example, once a day (specifying DEFAULT_DAYS_OFFSET = 1)

### Slack notification example: ###
![Slack notification](https://api.monosnap.com/rpc/file/download?id=gZPdIoJqrsiHcxPebpuC9KglbHmNFI)