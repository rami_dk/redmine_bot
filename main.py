import gettext
import logging
from logging import config

from redmine.data import redmine_data
from settings import LOGGING, LOCALE_PATH, LOCALE
from slack.client import slack_client
from slack.messages import IssuesMessage

if __name__ == '__main__':
    config.dictConfig(LOGGING)
    translation = gettext.translation('messages', LOCALE_PATH, [LOCALE], fallback=True)
    translation.install()

    try:
        for user in redmine_data.users_to_notify():
            issues = user.time_entry_requiring_issues()
            if issues:
                username = slack_client.get_username_by_email(user.mail)

                message = IssuesMessage(issues=issues, text=_('*Please, create time entries for the following tasks:*'))

                message_kwargs = {
                    'username': '@{}'.format(username),
                    'text': message.prepared()
                }
                logging.debug(
                    '\nNotification sent: \nuser: {username} \nmessage: {text}.'.format(**message_kwargs))
                slack_client.send_message(**message_kwargs)
    except Exception:
        logging.exception('Error sending notifications.')
