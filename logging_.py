import logging

import settings


class RequireLoggingLevelDebug(logging.Filter):
    def filter(self, record):
        return settings.LOGGING_LEVEL == 'DEBUG'
